<?php

    namespace Atarka\DJEM2\Models;

    use Atarka\DJEM2\Doctypes\Doctype;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Collection;
    use \ErrorException;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class Builder {
        protected $tableDocuments = 'documents';
        protected $whereConditions = [];
        protected $pathConditions = [];
        protected $sortOptions = [];
        protected $limitTake = false;
        protected $limitSkip = false;
        protected $withTypeLock = true;
        protected $model = Document::class;
        protected $query = null;
        protected $withDiscovery = false;

        public function __construct($model) {
            $this->model = $model;
        }

        public function find($id, $withDiscovery = false) {
            if (is_numeric($id)) {
                $this->where('_id', $id);

            } else if (is_array($id)) {
                $this->where('_url', 'in', $id);

            } else if (substr($id, 0, 1) === '/') {
                $this->where('_url', $id);

            } else {
                $this->where('_path', $id);
            }

            $this->discovery($withDiscovery);

            return $this->first();
        }

        public function typeLock($typeLock) {
            $this->withTypeLock = $typeLock;

            return $this;
        }

        public function discovery($withDiscovery) {
            $this->withDiscovery = $withDiscovery;

            return $this;
        }


        public function findOrFail($id) {
            $document = $this->find($id);
            if (!$document) {
                throw (new ModelNotFoundException)->setModel($this->model);
            }

            return $document;
        }

        public function first() {
            $this->take(1);

            return $this->get()->get(0);
        }



        public function getDocumentTypeCodes() {
            return Cache::remember('document_type_codes', 1, function() {
                $dir = new \DirectoryIterator(app_path('Models'));
                $documentTypes = [];

                foreach ($dir as $classFile) {
                    if (!$classFile->isFile()) continue;

                    try {
                        $class = '\\App\\Models\\' . str_replace('.php', '', $classFile->getFilename());
                        if (method_exists($class, 'getTypeCode')) {
                            $modelCode = $class::getTypeCode();
                            if (!empty($modelCode)) {
                                {
                                    $documentTypes[$modelCode] = $class;
                                }
                            }
                        }
                    } catch (\ErrorException $e) {
                    }
                }

                if (count($documentTypes)) {
                    $query = Document::path('settings.types.$')->where('_code', 'in', array_keys($documentTypes));

                    foreach ($query->get() as $q) {
                        $documentTypes[$q->id] = $documentTypes[$q->_code];
                    }
                }

                return $documentTypes;
            });
        }

        protected $documentTypeCodes = null;

        public function getModelByTypeId($typeId) {
            if ($this->documentTypeCodes === null) {
                $this->documentTypeCodes = $this->getDocumentTypeCodes();
            }

            return $this->documentTypeCodes[$typeId] ?? $this->model;
        }


        public function get() {
            $query = $this->getQuery();

            return $query->get()->map(function($row) {
                if ($this->withDiscovery) {
                    $model = $this->getModelByTypeId($row->document_type);
                    return $model::load($row);

                } else {
                    return $this->model::load($row);
                }
                // return Document::load($row); // new $this->model($row);
            });
        }


        public function count() {
            if (!$this->query) {
                $this->getQuery();
            }

            return $this->query->count();
        }


        public function resolveWhere($query, $whereConditions) {

            $gotDeletedCondition = false;
            foreach ($whereConditions as $condition) {
                if (is_array($condition[0])) {
                    $method = ($condition[3] ?? 'and') == 'or' ? 'orWhere' : 'where';
                    $query->$method(function($query) use ($condition) {
                        $this->resolveWhere($query, $condition[0]);
                    });

                } else {
                    if ($condition[1] === 'in') {
                        $query->whereIn($condition[0], is_array($condition[2]) ? $condition[2] : explode(',', $condition[2]));

                    } else {
                        $method = ($condition[3] ?? 'and') == 'or' ? 'orWhere' : 'where';
                        $query->$method($condition[0], $condition[1], $condition[2]);
                    }

                    if ($condition[0] == 'document_deleted') {
                        $gotDeletedCondition = true;
                    }
                }
            }

            return $gotDeletedCondition;
        }

        public function getQuery() {
            if (empty($this->pathConditions)) {
                if (!empty($this->djemPath)) {
                    $this->path($this->djemPath);
                }
            }

            $this->query = DB::table($this->tableDocuments);

            $whereConditions = array_merge([[$this->pathConditions, '', null, 'and']], $this->whereConditions);
            if (!empty($this->model::$typeId)) {
                $whereConditions[] = ['document_type', '=', $this->model::$typeId];
            }

            if ($this->withTypeLock && $typeCode = $this->model::getTypeCode()) {
                if ($typeCode != 'document') {
                    $typeId = Doctype::getTypeIdByCode($typeCode);
                    if ($typeId) {
                        $whereConditions[] = ['document_type', '=', $typeId];
                    }
                }
            }

            $gotDeletedCondition = $this->resolveWhere($this->query, $whereConditions);

            if (!$gotDeletedCondition) {
                $this->query->where('document_deleted', '=', 0);
            }

            /*
            $gotDeletedCondition = false;
            foreach ($whereConditions as $condition) {
                if ($condition[1] === 'in') {
                    $this->query->whereIn($condition[0], is_array($condition[2]) ? $condition[2] : explode(',', $condition[2]));

                } else {
                    $this->query->where($condition[0], $condition[1], $condition[2]);
                }

                if ($condition[0] == 'document_deleted') {
                    $gotDeletedCondition = true;
                }
            }

            if (!$gotDeletedCondition) {
                $this->query->where('document_deleted', '=', 0);
            }
*/

            foreach ($this->sortOptions as $sortOption) {
                $this->query->orderBy($sortOption[0], $sortOption[1] ?? 'asc');
            }

            if ($this->limitSkip) $this->query->skip($this->limitSkip);
            if ($this->limitTake) $this->query->take($this->limitTake);

            return $this->query;
        }



        function path($path) {
            $result = array();

            foreach (explode(',', $path) as $pathItem) {
                $pathItem = trim($pathItem);
                preg_match("/^(.+?)(\\.\\$|\\.\\*)*$/", $pathItem, $m);

                if (empty($m[2])) {
                    if($m[1] == '*') {
                        continue;
                    } elseif(is_numeric($m[1])) {
                        $result[] = ['document_id', '=', $pathItem, 'or'];
                    } else {
                        $result[] = ['document_path', '=', $pathItem, 'or'];
                    }

                } else {
                    if ($m[2] == '.$') {
                        if (is_numeric($m[1])) {
                            $result[] = ['document_parent_id', '=', $m[1]];

                        } else {
                            $parentQuery = DB::table($this->tableDocuments)->whereDocumentPath($m[1])->whereDocumentDeleted(0)->first();
                            if ($parentQuery) {
                                $result[] = ['document_parent_id', '=', $parentQuery->document_id, 'or'];
                            } else {
                                throw new ErrorException('Document with path "' . $m[1] . '" not found');
                            }
                        }

                    } elseif ($m[2] == '.*') {
                        if (is_numeric($m[1])) {
                            $parentQuery = DB::table($this->tableDocuments)->whereDocumentId($m[1])->whereDocumentDeleted(0)->first();
                            if ($parentQuery) {
                                $result[] = ['document_path', 'like', $parentQuery->document_path . '.%', 'or'];
                            }

                        } else {
                            $result[] = ['document_path', 'like', $m[1] . '.%', 'or'];
                        }

                    } else {
                        continue;
                    }
                }
            }

            $this->query = null;  // Сбрасываем результат выборки при изменении пути
            $this->pathConditions = $result;

            return $this;
        }


        public function take($take) {
            $this->limitTake = $take;

            return $this;
        }

        public function skip($skip) {
            $this->limitSkip = $skip;

            return $this;
        }

        public function getWhereConditions() {
            return $this->whereConditions;
        }

        public function where($what, $expression = '=', $value = null, $modifier = 'and') {
            if (is_callable($what)) {
                $builder = new static('');
                $what($builder);
                $this->whereConditions[] = [
                    $builder->getWhereConditions(),
                    '',
                    null,
                    $modifier];

            } else {
                if ($value === null) {
                    $value = $expression;
                    $expression = '=';
                }

                if ($expression === 'has') {
                    $this->whereConditions[] = [
                        DB::raw('FIND_IN_SET(' . $value . ', ' . $this->DJEMFieldName($what, 'documents', false, true) . ')'),
                        '>',
                        0
                    ];


                } else {
                    $this->whereConditions[] = [$this->DJEMFieldName($what), $expression, $value, $modifier];
                }
            }

            return $this;
        }

        public function orWhere($what, $expression, $value = null) {
            return $this->where($what, $expression, $value, 'or');
        }


        public function orderBy($sort) {
            $this->sortOptions = [];

            if ($sort) {
                foreach (explode(',', $sort) as $sortItem) {
                    $sortItem = trim($sortItem);

                    $sortOrder = 'asc';
                    $isDigital = false;

                    while ($sortItem) {
                        if ($sortItem[0] == '#') {
                            $isDigital = true;
                            $sortItem = substr($sortItem, 1);
                            continue;
                        }
                        if ($sortItem[0] == '-') {
                            $sortOrder = 'desc';
                            $sortItem = substr($sortItem, 1);
                            continue;
                        }
                        break;
                    }

                    if (isset($this->djem->cacheTables[$sortItem])) {
                        $this->sortOptions[] = [$sortItem, $sortOrder];
                        if ($isDigital) {
                            // $this->joinCacheTables['icache_' . $sortItem] = 1;
                        } else {
                            // $this->joinCacheTables['scache_' . $sortItem] = 1;
                        }
                    } else {
                        if ($sortItem == '_random') {
                            $this->sortOptions[] = [DB::raw('RAND()')];
                        } else {
                            $this->sortOptions[] = [$this->DJEMFieldName($sortItem, $this->tableDocuments, $isDigital), $sortOrder];
                        }
                    }
                }
            }

            $this->query = null;  // Сбрасываем результат выборки при изменении пути

            return $this;
        }


        protected function DJEMFieldName($fieldName, $tableName = 'documents', $isDigital = false, $skipDBRaw = false) {
            if ($fieldName[0] == '_') {
                return 'document' . $fieldName;
            }

            if (strpos($fieldName, '.') !== false) {
                return $fieldName;
            }

            $field = "IF(LOCATE('<" . $fieldName . ">', `$tableName`.document_content), SUBSTRING_INDEX(SUBSTRING_INDEX(`$tableName`.document_content, '<" . $fieldName . ">', -1), '</" . $fieldName . ">', 1), '')";

            return $skipDBRaw ? $field : DB::raw($field);
        }


        public function toArray() {
            $result = [];

            foreach ($this->get() as $item) $result[] = $item;

            return $result;
        }

    }