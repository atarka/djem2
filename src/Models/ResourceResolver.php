<?php

    namespace Atarka\DJEM2\Models;

    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\DB;

    class ResourceResolver {

        static $typeResources = null;

        public function getCtplId($doc) {
            if ($doc->_ctpl_id > 1) {
                return $doc->_ctpl_id;

            } else if ($doc->_ctpl_id == 0) {
                return $doc->_id;

            } else {
                if (static::$typeResources === null) {
                    static::$typeResources = Cache::remember('djem2:type_resources', 30, function() {
                        $typeResources = [];
                        foreach (DB::table('documents')->where('document_parent_id', 32)->get() as $type) {
                            $typeData = [];
                            if ($type->document_ctpl_id > 1) {
                                $typeData['ctpl'] = $type->document_ctpl_id;
                            } else if ($type->document_ctpl_id == 1) {
                                $typeData['ctpl'] = $type->document_id;
                            }

                            $typeResources[$type->document_id] = $typeData;
                        }
                        return $typeResources;
                    });
                }

                return static::$typeResources[$doc->_type]['ctpl'] ?? $doc->id;
            }
        }

        public function getCtplCode($doc) {

        }

        public function getCtpl($doc) {

        }


        protected function extractMultiLanguageFields($ctplId) {
            $multiLanguageFields = [];
            $ctplDoc = Document::find($ctplId);
            if (!$ctplDoc) return $multiLanguageFields;
            $ctpl = new DJEMXml($ctplDoc->_ctpl);

            foreach ($ctpl->__xml as $key => $node) {
                if ($node->type == 14) {
                    $multiLanguageFields = array_merge(
                        $multiLanguageFields,
                        $this->extractMultiLanguageFields(intval($node->code))
                    );
                    continue;
                }

                if (empty($node->multilanguage)) continue;
                $multiLanguageFields[] = strval($node->code);
            }

            return $multiLanguageFields;
        }


        public function getMultiLanguageFields($doc) {
            $ctplId = $this->getCtplId($doc);

            return Cache::remember('djem2:ctpl' . $ctplId . ':language_fields', 60, function() use ($ctplId) {
                return $this->extractMultiLanguageFields($ctplId);
            });
        }
    }