<?php

    namespace Atarka\DJEM2\Models;

    use Illuminate\Database\Eloquent\Model;

    class DJEMGrid extends Model {
        protected $table = 'grids';
        public $timestamps = false;
        protected $guarded = [];

    }
