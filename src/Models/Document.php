<?php

    namespace Atarka\DJEM2\Models;

    use App\Models\Traits\Ownable;

    class Document extends Model {
        use Ownable;

        public function getParentAttribute() {
            return static::find($this->_parent_id);
        }

        public function setParentAttribute($parentId) {
            if (is_string($parentId)) {
                $parent = \Atarka\DJEM2\Models\Document::find($parentId);

                if ($parent) {
                    $this->_parent_id = $parent->id;
                }
            } else if (is_numeric($parentId)) {
                $this->_parent_id = $parentId;
            }
        }

        public function setTypeAttribute($typeId) {
            if (is_string($typeId)) {
                $type = Document::find("settings.types.$typeId");
                if ($type) {
                    $this->_type = $type->id;
                }
            }
        }
    }