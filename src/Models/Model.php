<?php

    namespace Atarka\DJEM2\Models;

    use \Exception;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Str;

    use Atarka\DJEM2\Doctypes\Doctype;


    /**
     * Class Model
     * @package App\Models\DJEM2
     * @method static findOrFail(mixed $id)
     * @method static Builder path(string $path)
     * @method static Builder where(string $field, string $condition, mixed $value = null)
     * @method static Builder orderBy(string $order)
     */

    class Model {
        public $id = false;
        protected $systemFields = [];
        protected $changedFields = [];
        protected $__xml = null;
        protected $pendingAttributes = [];
        protected $djemPath = false;
        protected $cachedMutatorsValues = [];
        protected $docType = null;
        protected $table = 'documents';
        protected $tableAcls = 'acls';
        protected $row = null;

        public static $typeId;

        public function __construct($row = null, $docType = Doctype::class) {
            if ($row) {
                if ($row instanceof Model) {
                    $this->loadData($row->getData());

                } else {
                    $this->loadData($row);
                }
            } else {
                $this->__xml = new DJEMXml('<root></root>');
            }

            $this->docType = $docType;
        }

        public function newQuery() {
            $query = $builder = new Builder(new static);
            if ($this->djemPath) $query->path($this->djemPath);

            return $query;
        }

        public function getData() {
            return $this->row;
        }


        public static function load($row, $forcedModel = null) {
            $docType = Doctype::getDoctype($row->document_type);
            $model = $forcedModel ?? $docType::getModel() ?? get_called_class();

            return new $model($row, $docType);
        }


        protected function loadData($row) {
            $this->row = $row;

            if ($row->document_content) {
                try {
                    $this->__xml = new DJEMXml($row->document_content);
                } catch (\ErrorException $e) {
                    print $row->document_id . ' - ' . $e->getMessage();
                    exit;
                }
            } else {
                $this->__xml = null;
            }

            foreach ($row as $key => $value) {
                if ($key == 'document_content') continue;
                if ($key == 'document_id') {
                    $this->id = $value;
                }

                $xmlKey = substr($key, 8);
                if ($xmlKey) {
                    $this->systemFields[$xmlKey] = $value;
                }
            }
        }

        public function level($level) {
            if ($this->_level === $level) return $this;
            if ($this->_level < $level) return null;

            $levelPath = join('.', array_slice(explode('.', $this->_path), 0, $level + 1));
            return Document::find($levelPath, false);
        }


        public static function getTypeCode() {
            return 'document';
        }

        public function getTypeAttribute() {
            return new $this->docType($this);
        }

        public function getDocumentTypeAttribute() {
            return static::find($this->_type);
        }

        function __isset($key) {
            if (method_exists($this, 'get' . Str::studly($key).'Attribute')) {
                return true;

            } else if ($key[0] == '_') {
                return isset($this->systemFields[$key]);

            } else {
                return isset($this->__xml->$key);
            }
        }

        public function __get($key) {
            if (in_array($key, ['name', 'id', 'url'])) $key = '_' . $key;

            if ($key === '_type') {
                return $this->systemFields['_type'] ?? false;

            } else if (method_exists($this, 'get' . Str::studly($key).'Attribute')) {
                if (!isset($this->cachedMutatorsValues[$key])) {
                    $this->cachedMutatorsValues[$key] = $this->{'get' . Str::studly($key) . 'Attribute'}();
                }

                return $this->cachedMutatorsValues[$key];

            } else {
                // experimental language system
                return $this->getMultiLanguageValue($key);
            }
        }

        protected static $lang = null;
        protected $multiLanguageFields = null;

        protected function getMultiLanguageValue($key) {
            if ($key[0] == '_' && $key != '_name') {
                return $this->systemFields[$key] ?? '';
            }

            if ($this->multiLanguageFields === null) {
                $resolver = new ResourceResolver();
                $this->multiLanguageFields = $resolver->getMultiLanguageFields($this);
            }
            if (static::$lang === null) static::$lang = App::getLocale();

            if (in_array($key, $this->multiLanguageFields)) {
                $languageKey = static::$lang == 'ru' ? $key : static::$lang . ($key[0] == '_' ? '' : '_') . $key;
                if ($languageKey[0] == '_') {
                    return $this->systemFields[$languageKey] ?? '';

                } else {
                    return $this->__xml->$languageKey ?? ($key[0] == '_' ? $this->systemFields[$key] : $this->__xml->$key) ?? '';
                }

            } else {
                if ($key[0] == '_') {
                    return $this->systemFields[$key] ?? '';

                } else {
                    return $this->__xml->$key ?? '';
                }
            }
        }


        /*
        protected static $multiLanguageFields = null;
        protected function isMultiLanguageField($name) {
            if (static::$multiLanguageFields === null) {

            }
        }
        */

        public function __set($key, $value) {
            if ($key[0] == '_') {
                /*
                if ($key == '_parent_id') {
                    if (!empty($this->systemFields[$key]) && !$this->scheduledMove) {
                        $this->scheduledMove = $this->systemFields[$key];
                    }
                }
                */

                $this->systemFields[$key] = $value;
            } else if (method_exists($this, 'set' . Str::studly($key).'Attribute')) {
                $this->{'set' . Str::studly($key) . 'Attribute'}($value);

            } else {
                $this->__xml->$key = $value;
            }

            $this->changedFields[$key] = true;
        }


        public function save() {
            return DB::transaction(function() {
                $isContentUpdated = false;

                if (!$this->id) {
                    $this->insertDocument();
                }

                $updateValues = [];

                foreach (array_keys($this->changedFields) as $key) {
                    if ($key[0] === '_') {
                        $updateValues['document' . $key] = $this->systemFields[$key];

                    } else {
                        $isContentUpdated = true;
                        // update cache tables here
                    }
                }

                if ($isContentUpdated) {
                    $updateValues['document_content'] = $this->__xml->Write();
                }

                if (count($updateValues)) {
                    DB::table($this->table)->where('document_id', $this->id)->update($updateValues);
                }

                return $this->id;
            });
        }


        protected function insertDocument() {
            if (!$this->_parent_id) {
                throw new Exception('Parent document not set');
            }

            $parent = Model::find($this->_parent_id, false);
            $data = [
                '_create_time' => time(),
                '_name' => 'New document',
                '_ctpl' => '<root></root>',
                '_dtpl' => '',
                '_creator_id' => $parent->_creator_id,
                '_level'=> $parent->_level + 1,
                '_ctpl_id' => 1,
                '_dtpl_id' => 1,
                '_image' => 1,
                '_codepage' => 1,
            ];

            foreach ($data as $field => $value) {
                if (!isset($this->systemFields[$field])) {
                    $this->$field = $value;
                }
            }

            $this->fixDocumentType($parent);
            $this->checkForSameCode();

            $this->id = DB::table($this->table)->insertGetId([
                'document_parent_id' => $this->_parent_id,
                'document_type' => $this->_type,
                'document_name' => $this->_name,
                'document_dtpl' => $this->_dtpl,
                'document_ctpl' => $this->_ctpl,
                'document_content' => '<root></root>',
                'document_access' => '',
                'document_plugin' => '',
                'document_links' => '',
            ]);

            $this->systemFields['_id'] = $this->id;
            $this->_code = $this->systemFields['_code'] ?? $this->id;

            if ($this->isFolder()) {
                $parent->_sub_folders = $parent->_sub_folders + 1;
                $parent->save();

                $query = DB::table($this->tableAcls)->where('acl_document_id', $parent->id);
                foreach ($query->get() as $acl) {
                    DB::table($this->tableAcls)->insert([
                        'acl_user_id' => $acl->acl_user_id,
                        'acl_document_id' => $this->id,
                        'acl_access' => $acl->acl_access
                    ]);
                }

                if (empty($this->_url)) {
                    $this->_url = preg_replace('#(.+)/.*#', '$1', $parent->_url) . '/' . $this->_code . '/index.php';
                }

            } else {
                $parent->_sub_documents = $parent->_sub_documents + 1;
                $parent->save();

                if (empty($this->_url)) {
                    $this->_url = preg_replace('#(.+)/.*#', '$1', $parent->_url) . '/' . $this->_code . '.php';
                }
            }

            $this->_file = $this->systemFields['_file'] ?? $this->_url;
            if (empty($this->_default_subtype)) {
                if (!empty($this->_subtypes)) {
                    $this->_default_subtype = intval($this->_subtypes);
                }
            }

            $this->_path = $parent->_path . '.' . $this->_code;
            $this->_sort = $this->id;
            $this->_gsort = $parent->_gsort . $this->base36($this->_sort);
        }


        protected function base36($int) {
            $res = '0000';

            if ($int < 0 || $int > 1679615) return $res;
            $letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            for ($i=3; $int > 0; --$i) {
                $res[$i] = $letters[ $int % 36 ];
                $int = floor( $int / 36 );
            }

            return $res;
        }


        protected function isFolder() {
            if (!empty($this->_subtypes)) return true;

            $doc = DB::table($this->table . ' AS d')
                ->join($this->table . ' AS t', 't.document_id', '=', 'd.document_type')
                ->selectRaw('d.document_sub_documents + d.document_sub_folders AS kids')
                ->selectRaw('d.document_subtypes AS document_subtypes')
                ->selectRaw('t.document_subtypes AS type_subtypes')
                ->where('d.document_id', $this->id)
                ->first();

            if ($doc) {
                if ($doc->kids) return true;
                if ($doc->document_subtypes) return true;
                if ($doc->type_subtypes) return true;
            }

            return false;
        }

        protected function fixDocumentType($parent) {
            if (!empty($this->_type)) {
                if ($this->_type != 1) {
                    $typeDoc = static::find($this->_type, false);
                    if (strpos($typeDoc->_path, 'settings.types.') !== 0) {
                        throw new Exception('Illegal type');
                    }
                }

            } else {
                if ($parent->_default_subtype) {
                    $this->_type = $parent->_default_subtype;

                } else if ($parent->_subtypes) {
                    $this->_type = intval($parent->_subtypes);

                } else {
                    $parentTypeDoc = static::find($parent->_type, false);
                    if ($parentTypeDoc->_default_subtype) {
                        $this->_type = $parentTypeDoc->_default_subtype;

                    } else if ($parentTypeDoc->_subtypes) {
                        $this->_type = intval($parentTypeDoc->_subtypes);
                    }
                }

                if (!isset($this->_type)) {  // Ежели все еще пустой - то пусть будет весь в отца
                    $this->_type = $parent->_type;
                }
            }
        }


        protected function checkForSameCode() {
            if (isset($this->_code)) {
                $query = static::where('_code', $this->_code)->where('_parent_id', $this->_parent_id);
                if ($query->first()) {
                    throw new Exception('Document code already exists');
                }
            }
        }


        public function getValues($list = null) {
            $values = [];

            if ($list && is_array($list)) {
                foreach ($list as $key) {
                    $values[$key] = $this->$key;
                }

                return $values;

            } else {
                foreach ($this->__xml as $node) {
                    $values[$node->getName()] = strval($node);
                }

                return array_merge($values, $this->systemFields);
            }
        }

        /**
         * @param array $data
         */

        public function fill(array $data) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }

        public static function find($id, $withDiscovery = false) {
            $model = get_called_class();
            $builder = new Builder($model);

            return $builder->find($id, $withDiscovery);
        }

        public function __call($name, $arguments) {
            if (method_exists(Builder::class, $name)) {
                $model = get_called_class();
                $builder = new Builder($model);

                $builder = call_user_func_array([$builder, $name], $arguments);
                return $builder;
            }

            throw new \ErrorException('Method ' . self::class . '::' . $name . ' not found');
        }


        public static function __callStatic($name, $arguments) {
            if (method_exists(Builder::class, $name)) {
                $model = get_called_class();
                $builder = new Builder($model);

                $builder = call_user_func_array([$builder, $name], $arguments);
                return $builder;
            }

            throw new \ErrorException('Method ' . self::class . '::' . $name . ' not found');
        }
    }