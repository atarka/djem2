<?php

    namespace Atarka\DJEM2\Models;

    use \ArrayObject;
    use \ArrayIterator;
    use \IteratorAggregate;
    use \SimpleXMLElement;

    class DJEMXml extends ArrayObject  implements IteratorAggregate {
        public $__xml;
        public $__attributes;
        private $keys = null;
        private $withDJEMFeatures = true;

        function __construct($xml = false, $encoding = 'utf-8', $withDJEMFeatures = true) {
            if ($xml instanceof SimpleXMLElement) {
                $this->__xml = $xml;
            } elseif ($xml) {
                if (preg_match('#^[a-zA-Z_0-9:]+$#', $xml)) {
                    $xml = '<' . $xml . '></' . $xml . '>';
                }

                $this->Parse($xml, $encoding);
            } else {
                $this->Parse('<root></root>', $encoding);
            }

            $this->withDJEMFeatures = $withDJEMFeatures;
            $this->__attributes = false;
        }


        public function AddCData($key, $cdataText) {
            $this->__xml->$key = NULL;
            $node = dom_import_simplexml($this->__xml->$key);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($cdataText));
        }


        function Parse($xml, $encoding = 'utf-8') {
            if ($this->withDJEMFeatures) {
                $xml = str_replace('<$', '<__', $xml);
                $xml = str_replace('</$', '</__', $xml);
                $xml = preg_replace('#(<'.'/?)([0-9])#', '$1___$2', $xml);
                $xml = preg_replace('#(<'.'[a-zA-Z0-9_/]+):#', '$1___', $xml);
            }

            if (substr($xml, 0, 5) != '<?xml') $xml = '<?xml version="1.0" encoding="' . $encoding . "\"?>\n" . $xml;

            $this->__xml = simplexml_load_string($xml);
            if ($this->__xml === false) {
                $this->__xml = new SimpleXMLElement('<root></root>');
            }
        }

        function Keys() {
            return $this->__xml->children();
        }


        function GetName() {
            return $this->__xml->getName();
        }


        function Name() {
            return $this->__xml->getName();
        }


        function Text() {
            return strval($this->__xml);
        }


        function Insert($node) {
            $node1 = dom_import_simplexml($this->__xml);
            $dom_sxe = dom_import_simplexml($node->__xml);
            $node2 = $node1->ownerDocument->importNode($dom_sxe, true);
            $node1->appendChild($node2);
        }


        function Node($key) {
            $key = $this->_ClearKey($key);

            return new DJEMXml($this->__xml->$key);
        }

        function Nodes($key) {
            $key = $this->_ClearKey($key);
            $result = array();

            foreach ($this->__xml->children() as $c) {
                if ($c->getName() == $key) {
                    $result[] = new DJEMXml($c);
                }
            }

            return $result;
        }


        function NodesDeep($key, &$result) {

            foreach ($this->__xml->children() as $c) {
                $node = new DJEMXml($c);

                if ($c->getName() == $key) {
                    $result[] = $node;

                } else {
                    $node->NodesDeep($key, $result);
                }
            }

        }


        function Attributes() {
            if ($this->__attributes === false) {
                if ($this->__xml) {
                    $this->__attributes = $this->__xml->attributes();
                }
            }

            return $this->__attributes;
        }

        function Attribute($key, $value = NULL) {
            $this->Attributes();

            if (is_null($value)) {
                return strval($this->__attributes->$key);

            } else {
                if (!empty($this->__attributes->$key)) {
                    $this->__attributes->$key = $value;
                } else {
                    $this->__xml->addAttribute($key, $value);
                }
            }
        }




        function __toString() {
            return $this->Write();
        }


        function ToArray($simpleXml = false) {
            $result = array();

            if ($simpleXml === false) $simpleXml = $this->__xml;
            if (!$simpleXml) return '';
            $keys = array();

            foreach ($simpleXml as $key => $node) {
                $key = $this->_UnescapeKey($key);
                $nodeContents = $this->ToArray($node);

                if (count($nodeContents) == 0) {
                    $nodeContents = (string) $node;
                }

                if (isset($keys[$key])) {
                    switch ($keys[$key]) {
                        case 1:
                            $result[$key] = array($result[$key]);
                            $keys[$key] = 2;

                        case 2:
                            $result[$key][] = $nodeContents;
                            break;
                    }
                } else {
                    $result[$key] = $nodeContents;
                    $keys[$key] = 1;
                }
            }

            return $result;
        }


        function _ClearKey($key) {
            if ($key[0] == '$') $key = '__' . substr($key, 1);
            $key = str_replace(':', '___', $key);

            return $key;
        }


        function _UnescapeKey($key) {
            if ($this->withDJEMFeatures) {
                if ($key[0] == '_') {
                    $key = str_replace('___', '', $key);
                    $key = str_replace('__', '$', $key);
                } else {
                    $key = str_replace('___', ':', $key);
                }
            }

            return $key;
        }



        function __call($key, $args) {
            $key = $this->_ClearKey($key);

            return new DJEMXml($this->__xml->$key);
        }


        public function __isset($key) {
            return isset($this->__xml->{ $this->_ClearKey($key) });
        }


        function __get($key) {
            $key = $this->_ClearKey($key);

            return strval($this->__xml->$key);
        }


        function __set($key, $value) {
            if ($key[0] == '$') $key = '__' . substr($key, 1);
            $key = str_replace(':', '___', $key);

            if (isset($this->__xml->$key)) {
                unset($this->__xml->$key);
            }

            $this->__xml->$key = $value;
        }


        function Reset($key) {
            $key = $this->_ClearKey($key);

            unset($this->__xml->$key);
        }


        function Write($withHeader = false, $beautiful = false) {
            $xml = $this->__xml->asXML();

            if ($this->withDJEMFeatures) {
                $xml = str_replace('<___', '<', $xml);
                $xml = str_replace('</___', '</', $xml);
                $xml = str_replace('<__', '<$', $xml);
                $xml = str_replace('</__', '</$', $xml);
                $xml = preg_replace('#(<'.'[a-zA-Z0-9_/]+)___#', '$1:', $xml);
                $xml = preg_replace('#<([^> ]+)([^>]*)/>#s', '<$1$2></$1>', $xml);
            }

            if ($withHeader === false) {
                $xml = preg_replace('#<\?xml.+?\?>[\r\n]*#s', '', $xml);
            }

            if ($beautiful) {
                $domxml = new DOMDocument('1.0');
                $domxml->preserveWhiteSpace = false;
                $domxml->formatOutput = true;
                $domxml->loadXML($xml);
                $xml = $domxml->saveXML();
            }

            return $xml;
        }

        // Реализация  ArrayObject
        function offsetGet($key) {
            return $this->Attribute($key);
        }


        function offsetSet($key, $value) {
            $this->Attribute($key, $value);
        }



        // Реализация интерфейса Iterator
        function rewind() {
            $this->keys = array();

            foreach ($this->Keys() as $k) {
                $this->keys[] = strval($k->getName());
            };

            return current($this->keys);
        }

        function current() {
            return $this->{current($this->keys)}; // $this->_doc;
        }

        function key() {
            return current($this->keys);
        }

        function next() {
            return next($this->keys); // $this->Fetch();
        }

        function valid() {
            return current($this->keys) !== false;
        }

        public function getIterator() {
            $this->keys = array();

            foreach ($this->Keys() as $k) {
                $this->keys[$k->getName()] = $k;
            };

            return new ArrayIterator($this->keys);
        }
    }