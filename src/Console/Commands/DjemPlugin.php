<?php

namespace Atarka\DJEM2\Console\Commands;

use Atarka\DJEM2\Plugins\Generic;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Atarka\DJEM2\Models\DJEMXml;
use \Exception;

class DjemPlugin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'djem:plugin {plugin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Interface for DJEM plugins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $input = file_get_contents('php://stdin');
        $xml = new DJEMXml($input);
        $pluginClass = '\App\Plugins\\' . Str::studly($this->argument('plugin'));
        if (class_exists($pluginClass)) {
            Log::stack(['plugin'])->info('runneth');
            $plugin = new $pluginClass();
            try {
                $result = $plugin->handle($xml);
            } catch (\Exception $e) {
                Log::stack(['plugin'])->error($e->getMessage());
            }

            if ($result) {
                print $result;
            }
        }
    }
}
