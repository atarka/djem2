<?php

    namespace Atarka\DJEM2;

    use Atarka\DJEM2\Console\Commands\DjemPlugin;
    use Illuminate\Support\ServiceProvider;

    class DJEM2ServiceProvider extends ServiceProvider {
        public function boot() {
            $this->loadRoutesFrom(__DIR__.'/routes/web.php');
            if ($this->app->runningInConsole()) {
                $this->commands([
                    DjemPlugin::class,
                ]);
            }
        }

        public function register() {

        }
    }