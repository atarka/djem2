<?php

    namespace Atarka\DJEM2\Doctypes;

    use Atarka\DJEM2\Models\Document;
    use Illuminate\Support\Str;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Session;

    class Doctype {
        protected $template = null;
        protected $document = null;
        protected static $model = null;

        public function __construct($document = null, $template = null, $model = null) {
            $this->setDocument($document);

            if ($template) {
                $this->template = $template;
            }
        }

        public function setDocument($document) {
            if (static::$model) {
                if ($document) {
                    if ($document instanceof static::$model) {
                        $this->document = $document;
                    } else {
                        exit;
                        $this->document = static::$model::find($document->id);
                    }

                } else {
                    $this->document = $document;
                }

            } else {
                $this->document = $document;
            }
        }


        public function getTemplate($request) {
            if ($this->template) {
                return $this->template;
            }

            $code = null;
            if ($this->document->_dtpl_id > 1) {
                $code = $this->getDesignTemplateCode($this->document->_dtpl_id);

            } else {
                $docType = Document::find($this->document->_type);
                if ($docType->_dtpl_id > 1) {
                    $code = $this->getDesignTemplateCode($docType->_dtpl_id);
                }
            }

            return $code ? "pages.$code" : null;
        }


        public function getDesignTemplateCode($id) {
            return Document::find($id)->_code ?? false;
        }


        public function getData($request) {
            return [];
        }


        public function view($request) {
            $template = $this->getTemplate($request);

            if (empty($template)) {
                $simpletons = [
                ];

                if (empty($simpletons[$this->document->_type])) {
                    print 'Not found for ' . $this->document->_type;
                    exit;

                } else {
                    $template = $simpletons[$this->document->_type];
                }
            }

            $data = ['doc' => $this->document] + $this->getData($request);
            $dataFunctionName = 'getData' . Str::studly(str_replace('pages.', '', $template));
            if (method_exists($this, $dataFunctionName)) {
                //$data += $this->$dataFunctionName($request);
                $data = array_merge($data, $this->$dataFunctionName($request));
            }

            return view($template, $data);
        }

        public static function getModel() {
            return static::$model;
        }

        protected static $typeCodes = null;
        protected static $typeCode2Id = null;
        protected static function getTypeCodes($code2id = false) {
            if (static::$typeCodes === null) {
                static::$typeCodes = Cache::remember('djem2:document_types', 30, function() {
                    $typeCodes = [];
                    $query = DB::table('documents')->where('document_path', 'like', 'settings.types.%');

                    foreach ($query->get() as $q) {
                        $typeCodes[$q->document_id] = $q->document_code;
                    }

                    return $typeCodes;
                });

                static::$typeCode2Id = [];
                foreach (static::$typeCodes as $id => $code) {
                    static::$typeCode2Id[$code] = $id;
                }
            }

            return $code2id ? static::$typeCode2Id : static::$typeCodes;
        }

        public static function getTypeIdByCode($code) {
            return (static::getTypeCodes(true))[$code] ?? 0;
        }

        public static function getDoctype($typeId) {
            $typeCode = (static::getTypeCodes())[$typeId] ?? $typeId;

            $class = "\\App\\Doctypes\\" . Str::studly($typeCode);
            if (class_exists($class)) {
                return $class;
            }

            return static::class;
        }
    }