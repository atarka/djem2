<?php

    namespace Atarka\DJEM2\Plugins;

    use Atarka\DJEM2\Models\DJEMXml;
    use Atarka\DJEM2\Models\DJEMGrid;
    use Atarka\DJEM2\Models\Document;
    use Illuminate\Support\Facades\Log;

    class Generic {
        const TYPE_TEXT = 'text';
        protected $model = Document::class;

        public function log($str) {
            Log::stack(['plugin'])->info($str);
        }

        public function handle($xml) {
            $log = Log::stack(['plugin']); // ->info('help');
            $command = $xml->command;
            $log->info("Command inner: $command");

            $command = 'command' . $command;
            if (method_exists($this, $command)) {
                $result = $this->$command($xml);
                return $result;

            } else {
                return null;
            }
        }

        protected function getQuery($xml) {
            $query = new $this->model;
            $query = $query::take(500);

            return $query;
        }


        protected function getWhere(&$query, $xml) {
            $filter = $xml->Node('filter');
            if ($filter) {
                $root = $filter->Node('root');
                if ($root) {
                    if ($root->where) {
                        $operatorReplacements = [
                            'in' => 'in',
                            'has' => '=',
                            '==' => '=',
                            '=' => '=',
                            '!=' => '!=',
                            'lt' => '<',
                            'le' => '<=',
                            'gt' => '>',
                            'ge' => '>=',
                        ];

                        foreach (preg_split('# && #', $root->where) as $rule) {
                            if (preg_match('#([a-zA-Z0-9_\->]+) *(has|==|=|!=|in|lt|gt|ge|le) *(.+)#', $rule, $m)) {
                                list (, $field, $operator, $value) = $m;
                                if (!isset($operatorReplacements[$operator])) continue;
                                $query->where($field, $operatorReplacements[$operator], $value);
                                // print "$field, $operator (" . $operatorReplacements[$operator] . "), $value\n";
                            }
                        }
                    }
                }
            }

            return $query;
        }



        function getItemsCount($xml) {
            $query = $this->getQuery($xml);
            $query = $this->getWhere($query, $xml);

            return $query->count();
        }

        protected function getItems($xml) {
            $query = $this->getQuery($xml);
            $query = $this->getWhere($query, $xml);

            $limit = $xml->filter()->root()->limit;
            if ($limit) {
                list ($skip, $take) = explode(',', $limit);
                $query->skip($skip);
                $query->take($take);
            }

            $columns = $this->getColumns($xml);
            $sort = $xml->filter()->root()->sort;
            if (!empty($sort)) {
                $this->log('sort: ' . $sort);
                $sortOrder = 'asc';
                if ($sort[0] == '-') {
                    $sortOrder = 'desc';
                    $sort = substr($sort, 1);
                }

                foreach ($columns as $column) {
                    if ($column['name'] == $sort) {
                        if (preg_match('/<' . '#([a-zA-Z0-9_]+)#>/', $column['data'], $m)) {
                            $query->orderBy($m[1], $sortOrder);
                            $this->log('sorta by ' . $m[1]);
                        }
                    }
                }
            }

            $this->log('filter: ' . $xml->Write());

            $result = [];

            foreach ($query->get() as $q) {
                $row = ['id' => $q->id];

                foreach ($columns as $column) {
                    $row[$column['fieldname']] = preg_replace_callback('#<\#(.+?)\#>#si', function($m) use ($q) {
                        $value = $q;

                        foreach (explode('->', $m[1]) as $pathItem) {
                            $value = $value->$pathItem;
                            if (empty($value)) break;
                        }

                        return $value;

                    }, $column['data']);
                }

                $result[] = $row;
            }

            return $result;
        }

        public function commandGetDirectoryList($xml) {
            $items = $this->getItems($xml);

            $result = new DJEMXml('root');

            foreach ($items as $item) {
                $row = new DJEMXml('row');
                $row->_id = isset($item['id']) ? $item['id'] : '0';
                foreach ($item as $key => $value) {
                    $row->$key = $value;
                }

                $result->Insert($row);
            }

            return $result->Write();
        }


        protected function getColumns($xml) {
            $folderId = $xml->id;
            $folder = Document::find($folderId);

            $grid = DJEMGrid::where('grid_id', $folder->_grid_id)->first();
            $gridXml = new DJEMXml('<root>' . $grid->grid_header . '</root>');
            $gridRows = $gridXml->Nodes('row');
            $result = [];

            $cnt = 0;

            foreach ($gridRows as $gridRow) {
                $field = 'c' . $cnt++;
                $result[] = [
                    'name' => $field,
                    'fieldname' => $field,
                    'type' => $gridRow->type,
                    'desc' => $gridRow->desc,
                    'width' => $gridRow->width,
                    'readonly' => $gridRow->readonly,
                    'data' => $gridRow->data,
                ];
            }

            return $result;
        }

        function commandGetGridHeader($xml) {
            $totalItems = $this->getItemsCount($xml);
            $folderId = $xml->id;
            $folder = Document::find($folderId);

            $result = new DJEMXml('<root></root>');
            $result['totalItems'] = $totalItems;
            $result['total'] = $totalItems;
            $result['grid_id'] = $folder ? $folder->_grid_id : 0;

            foreach ($this->getColumns($xml) as $col) {
                $row = new DJEMXml('row');
                $row->name = $col['name'];
                $row->type = $col['type'];
                $row->desc = $col['desc'];
                $row->width = $col['width'];
                $row->fieldname = $col['name'];
                $row->readonly = '1';
                $result->Insert($row);
            }

            return $result->Write();
        }

        function commandDeleteDocument($xml) {
            $id = $xml->id;
            if (!empty($id)) {
                $query = new $this->model;
                // $query->where($query->getPrimaryKey(), 'in', explode(',', $id))->delete();
                $query->whereIn($query->getPrimaryKey(), explode(',', $id))->delete();
            }
        }

    }
