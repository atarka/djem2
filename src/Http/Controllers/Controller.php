<?php

    namespace Atarka\DJEM2\Http\Controllers;

    use Atarka\DJEM2\Models\Document;
    // use App\Models\RedirectItem;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\View;
    use Symfony\Component\HttpKernel\Exception\HttpException;

    class Controller extends BaseController
    {

        public function getGeneric(Request $request) {
            App::setLocale(\App\Lib\Lang::getLang()->id);
            View::share('ajaxPartial', !empty($request->header('ajax-partial')));
            View::share('authed', Auth::user());

            $url = $request->path();
            if ($url[0] != '/') $url = '/' . $url;

            try {
                // RedirectItem::checkRedirect($url);

                $doc = Document::find($url, true);
                if (!$doc) return app()->call('\App\Http\Controllers\Controller@getSitemap', ['404']); // throw new NotFoundHttpException('Not found');

                if ($doc->redirect && $doc->redirect != $doc->url) {
                    return redirect($doc->redirect);
                }

                return $doc->type->view($request);

            } catch (HttpException $e) {
                if ($e->getStatusCode() == 301) {
                    return redirect()->to($e->getMessage());
                }
            }
        }
    }
